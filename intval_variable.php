<?php
echo intval(42),"\n";
echo intval(4.2),"\n";
echo intval('42'),"\n";
echo intval('+42'),"\n";
echo intval('-42'),"\n";
echo intval(042),"\n";
echo intval(1e42),"\n";
//echo intval(0*1A);
echo intval(42000000000),"\n";
echo intval('420000000000000000000'),"\n";
echo intval(42, 8),"\n";
echo intval('42', 8),"\n";
echo intval(array()),"\n";
echo intval(array('foo', 'bar')),"\n";